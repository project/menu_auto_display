INTRODUCTION
------------

The Menu Auto Display module displays the title and description of child menu
entries when the parent menu is clicked. The display is generated from the 
child menu entries and permissions granted to the current user.

This module was inspired by system_admin_menu. It simplifies use of multi-level 
menus by generating a display for the parent level.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/menu_auto_display
   
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/menu_auto_display

REQUIREMENTS
------------

Core Menu module enabled.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

Once the module is turned on, the menu itself is used to invoke the module.
In the Edit Menu Link screen, set the Path to menu_auto_display/menu_link_title 
where "menu_link_title" follows the usual rules for machine name specification. 

For example, the Menu link title of "About us" is set to use the Path 
"menu_auto_display/about_us" to automatically display a link page. Everything
else is set up as usual.

To view the generated page, click the parent menu link instead of following the
dropdown. Using the same example, click on the "About us" menu entry instead of
a child entry. The generated page will show the child menu links shown to the
user, each with its menu description.

EXAMPLE
-------

For the example described below, Drupal 7 was installed with default options.
Then, Menu Auto Display was installed and enabled. Note that the "Navigation"
menu is displayed on the left side of the home page. Therefore, that menu is 
used in the example.

Entries in any menu may be configured as described.

Select: Structure > Menus
Select: Navigation > add link

Enter the following values into the form:

Menu link title:    About us
Path:               menu_auto_display/about_us
Description:        About us and how you can help

Click Save


Go to the Home page and select the "Add content" menu item. Add a Basic page
with the title "About our organization" and enter some text. Select URL path
settings and enter "about_our_organization". Click Save.

From the displayed page or the home page, select the "Add content" menu item.
Add a Basic page with the title "Current projects" and enter some text. 
Select URL path settings and enter "current_projects". Click Save.


Select: Structure > Menus
Select: Navigation > add link

Enter the following values into the form:

Menu link title:    About our organization
Path:               about_our_organization
Description:        Who we are and what we do
Parent link:        -- About us
                    (Found under the <Navigation> entry)

Click Save


Click Add link and enter the following values:

Menu link title:    Current projects
Path:               current_projects
Description:        Cool stuff we're doing now
Parent link:        -- About us
                    (Found under the <Navigation> entry)

Click Save


The menu link titles may differ from the titles used on Basic page entries.
The path values must match the URL path values.

Note that both of these entries are shown under the "About us" menu link. 
If desired, you may drag to change the order of the child links under that
parent.


Go to the home page and click the link "About us" in the Navigation menu.
You will see a generated page containing links to the menu entries, with
descriptions. The order of entries matches the order in the menu. If user
roles are used to restrict access to menu items, inaccessible entries will
not be displayed. If entries are added or removed, the generated page is
updated automatically.