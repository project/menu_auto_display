<?php
/**
 * @file
 * Callbacks for the menu_auto_display module.
 */

/**
 * Provide a single block from the site menu as a page.
 *
 * This function is the destination for top or mid-level menu items.
 * For example, the "About us" top-level menu needs to have a destination
 * to be valid in the Drupal menu system, so we automatically generate a
 * page of links to the child menu items, with their description.
 *
 * $args contains path element(s) after "menu_auto_display" and are used to
 * create the link path value used to build the page
 *
 * inspired by system_admin_menu_block_page
 *
 * @return string
 *   The output HTML.
 */
function menu_auto_display_block_page() {
  $args = func_get_args();
  $item = menu_get_item();
  $path = $item['path'];

  foreach ($args as $arg) {
    $path = $path . '/' . $arg;
  }
  $item['fullpath'] = $path;

  if ($content = menu_auto_display_block($item)) {
    $output = theme('menu_auto_display_block', $content);
  }
  else {
    $output = t('You do not have access to any menu items.');
  }
  drupal_set_title($item['title']);
  return $output;
}

/**
 * Provide a single block on the menu overview page.
 *
 * This is ALWAYS called to display info about the child links for THIS item
 * set up as in menu_auto_display_menu.
 *
 * Inspired by system_admin_menu_block
 *
 * @param array $item
 *   The menu item to be displayed.
 */
function menu_auto_display_block(array &$item) {
  if (!isset($item['mlid']) || !isset($item['title'])) {
    $result = db_query("SELECT mlid, menu_name, link_title FROM {menu_links} ml WHERE ml.link_path = :path", array(':path' => $item['fullpath']));
    $test = $result->fetchAssoc();
    if (!$test) {
      return FALSE;
    }
    $item['title'] = $test['link_title'];
    $item['mlid'] = $test['mlid'];
    $item['menu_name'] = $test['menu_name'];
  }

  $content = array();
  $query = db_select('menu_links', 'ml', array('fetch' => PDO::FETCH_ASSOC));
  $query->join('menu_router', 'm', 'm.path = ml.router_path');
  $query
    ->fields('ml')
    // Weight should be taken from {menu_links}, not {menu_router}.
    ->fields('m', array_diff(drupal_schema_fields_sql('menu_router'), array('weight')))
    ->condition('ml.plid', $item['mlid'])
    ->condition('ml.menu_name', $item['menu_name'])
    ->condition('ml.hidden', 0);

  foreach ($query->execute() as $link) {
    _menu_link_translate($link);
    if ($link['access']) {
      // The link description, either derived from 'description' in
      // hook_menu() or customized via menu module is used as title attribute.
      if (!empty($link['localized_options']['attributes']['title'])) {
        $link['description'] = $link['localized_options']['attributes']['title'];
        unset($link['localized_options']['attributes']['title']);
      }
      // Prepare for sorting as in function _menu_tree_check_access().
      // The weight is offset so it is always positive, with a uniform 5-digits.
      $key = (50000 + $link['weight']) . ' ' . drupal_strtolower($link['title']) . ' ' . $link['mlid'];
      $content[$key] = $link;
    }
  }
  ksort($content);

  // Mark OUR content.
  return array('menu_auto_display' => $content);
}

/**
 * Returns HTML for the content of a menu-auto-display block.
 *
 * Inspired by theme_admin_block_content.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array containing information about the block. Each element
 *     of the array represents an administrative menu item, and must at least
 *     contain the keys 'title', 'href', and 'localized_options', which are
 *     passed to l(). A 'description' key may also be provided.
 *
 * @ingroup themeable
 */
function theme_menu_auto_display_block(array $variables) {
  $output = '';

  $my_vars = $variables['menu_auto_display'];
  if (isset($my_vars) && !empty($my_vars)) {
    $class = 'menu-auto-display';
    $output .= '<dl class="' . $class . '">';
    foreach ($my_vars as $item) {
      if (!is_null($item) && !empty($item)) {
        $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
        if (isset($item['description'])) {
          $output .= '<dd>' . $item['description'] . '</dd>';
        }
      }
    }
    $output .= '</dl>';
  }
  return $output;
}
